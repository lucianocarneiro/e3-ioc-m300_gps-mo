require essioc
require m300_gps, 0.9.0+1


iocshLoad("$(essioc_DIR)/common_config.iocsh")

#------------------------------------------------------------------------------
# m300_gps MACROS
epicsEnvSet("P", "TRef-D1:")
epicsEnvSet("R", "Ctrl-GPS-001:")
epicsEnvSet("HOST", "172.16.6.39")
epicsEnvSet("MIBDIRS", "+$(m300_gps_DIR)")
epicsEnvSet("MIB", "MBG-SNMP-LTNG-MIB")
epicsEnvSet("COMMUNITY", "esstn")


#------------------------------------------------------------------------------
iocshLoad("$(m300_gps_DIR)/m300_gps.iocsh", "COMMUNITY=$(COMMUNITY), P=$(P), R=$(R), HOST=$(HOST), MIB=$(MIB)")
